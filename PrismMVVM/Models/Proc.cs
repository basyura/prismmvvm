﻿using System.Diagnostics;
using Prism.Mvvm;
using Reactive.Bindings;

namespace PrismMVVM.Models
{
    public class Proc : BindableBase
    {
        /// <summary></summary>
        private Process _process;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="process"></param>
        public Proc (Process process)
        {
            _process = process;

            Id = new ReactiveProperty<int>(_process.Id);
            ProcessName = new ReactiveProperty<string>(_process.ProcessName);
            BasePriority = new ReactiveProperty<int>(_process.BasePriority);

            ProcessPriorityClass priority = 0;
            try
            {
                priority = _process.PriorityClass;
            }
            catch
            {
            }
            PriorityClass = new ReactiveProperty<ProcessPriorityClass>(priority);
        }
        /// <summary></summary>
        public ReactiveProperty<int> Id { get; }
        /// <summary></summary>
        public ReactiveProperty<string> ProcessName { get; }
        /// <summary></summary>
        public ReactiveProperty<ProcessPriorityClass> PriorityClass { get; private set; }
        /// <summary></summary>
        public ReactiveProperty<int> BasePriority { get; }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="priority"></param>
        public void Change(ProcessPriorityClass priority)
        {
            _process.PriorityClass = priority;
            PriorityClass.Value = priority;
        }
    }
}
