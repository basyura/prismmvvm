﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Reactive.Linq;
using Prism.Mvvm;
using PrismMVVM.Models;
using Reactive.Bindings;

namespace PrismMVVM.ViewModels
{
    public class MainWindowViewModel : BindableBase
    {
        /// <summary>
        /// 
        /// </summary>
        public MainWindowViewModel()
        {
            Initialize();
        }

        /// <summary></summary>
        public ReactiveProperty<string> Title { get; } = new ReactiveProperty<string>("Prism + ReactiveProperty");
        /// <summary></summary>
        public ReactiveCollection<Proc> Processes { get; private set; }
        /// <summary></summary>
        public ReactiveProperty<Proc> SelectedProcess { get; } = new ReactiveProperty<Proc>();

        /// <summary></summary>
        public ReactiveCommand ChangePriorityCommand { get; } = new ReactiveCommand();

        /// <summary>
        /// 
        /// </summary>
        private void Initialize()
        {
            ChangePriorityCommand.Subscribe(_ => ChangePriority());

            //Process.GetCurrentProcess().BasePriority
            Processes = Process.GetProcesses().Where(v => v.ProcessName != "svchost")
                                              .OrderBy(v => v.ProcessName)
                                              .Select(v => new Proc(v))
                                              .ToObservable().ToReactiveCollection();

        }

        /// <summary>
        /// 
        /// </summary>
        private void ChangePriority()
        {
            Proc proc = SelectedProcess.Value;
            if (proc == null)
            {
                return;
            }

            proc.Change(ProcessPriorityClass.Idle);
        }
    }
}
